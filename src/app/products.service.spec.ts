import { TestBed } from '@angular/core/testing';

import { ProductsService } from './products.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';

describe('ProductsService', () => {
  let httpClientSpy;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientModule ]
  }));

  beforeEach(() => {    
    httpClientSpy = TestBed.get(HttpClient);
  });

  it('should be created', () => {
    const service: ProductsService = TestBed.get(ProductsService);
    expect(service).toBeTruthy();
  });
  
  it('should return data (httpClient.get call once) ', () => {
    spyOn(httpClientSpy, 'get');
    const service: ProductsService = TestBed.get(ProductsService);
    service.getProductDetails();
    expect(httpClientSpy.get).toHaveBeenCalled();
  });
});
