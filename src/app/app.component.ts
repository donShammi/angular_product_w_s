import { Component } from '@angular/core';
import { ProductsService } from './products.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
products: any = [];
showModal: boolean = false;
selectedProduct: any = {};

  constructor(private productsService: ProductsService) {

  }

  ngOnInit() {
    console.log('this.productsService====', this.productsService);
    this.productsService.getProductDetails().subscribe((resp) => {
      console.log('resp------', resp);
      this.products = resp['groups'];
    })
  }

  openModal(product){
    this.selectedProduct = product;
    this.showModal= true;
  }

}
