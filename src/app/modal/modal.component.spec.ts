import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalComponent } from './modal.component';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    
    component = new ModalComponent();
    // fixture = TestBed.createComponent(ModalComponent);
    //   component = fixture.componentInstance;
    component.product = {
      images: [
        {
            "size": "m",
            "meta": "",
            "alt": "",
            "rel": "althero",
            "width": 363,
            "href": "https://www.westelm.com/weimgs/rk/images/wcm/products/201848/0001/organic-luxe-fibrosoft-towels-gray-sky-melange-m.jpg",
            "height": 363
        },
        {
            "size": "m",
            "meta": "",
            "alt": "",
            "rel": "althero",
            "width": 363,
            "href": "https://www.westelm.com/weimgs/rk/images/wcm/products/201848/0001/organic-luxe-fibrosoft-towels-midnight-m.jpg",
            "height": 363
        },
        {
            "size": "m",
            "meta": "",
            "alt": "",
            "rel": "althero",
            "width": 363,
            "href": "https://www.westelm.com/weimgs/rk/images/wcm/products/201848/0001/organic-luxe-fibrosoft-towels-midnight-m.jpg",
            "height": 363
        }
      ]
    };
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should hide the modal', () => {
    spyOn(component.hideModal, 'next');
    component.closeModal();
    expect(component).toBeTruthy();
    expect(component.hideModal.next).toHaveBeenCalled();
  });

  it('should show the last image when clicking on prev if the index is 0', () => {
    component.leftClick();
    expect(component.currentIndex).toEqual(2);
  });

  it('should show the previous index image when clicking on prev', () => {
    component.currentIndex = 2;
    component.leftClick();
    expect(component.currentIndex).toEqual(1);
  });

  it('should update the currentIndex by incrementing the value 1', () => {
    component.rightClick();
    expect(component.currentIndex).toEqual(1);
  });

  it('should update the currIndex with 0, if the currentIndex and imageLength are equal', () => {
    component.currentIndex = 2;
    component.rightClick();
    expect(component.currentIndex).toEqual(0);
  });

});
