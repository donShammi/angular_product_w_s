import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
@Input() product: any ;
@Output() hideModal: EventEmitter<any> = new EventEmitter();
currentIndex: number = 0;
  constructor() { }

  ngOnInit() {
    console.log('product===', this.product)
  }


  closeModal(){
    this.hideModal.next();
  }
  leftClick(){
    this.currentIndex == 0 ? this.currentIndex = this.product.images.length - 1 : this.currentIndex--;
  }
  rightClick(){
  this.currentIndex == this.product.images.length - 1 ? this.currentIndex = 0 : this.currentIndex++;
  }
}
