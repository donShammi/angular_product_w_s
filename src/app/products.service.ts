import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private httpClient: HttpClient) { }


  getProductDetails(){
    return this.httpClient.get('../assets/data/products.json'); 
    //return this.httpClient.get('https://www.westelm.com/services/catalog/v4/category/shop/new/all-new/index.json');
  }
}
