import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ModalComponent } from './modal/modal.component';
import { ProductsService } from './products.service';
import { of } from 'rxjs';

describe('AppComponent', () => {
  let fixture, app;
  const productDetails = {
    groups: [
      {
        hero: {
            "href": "https://www.westelm.com/weimgs/rk/images/wcm/products/201848/0001/organic-luxe-fibrosoft-towels-white-1-m.jpg",
            "height": 363
        },
        priceRange: {
          selling: {
            high: 69
          }
        }
      }
    ]
  };
  class MockProductService { 
    getProductDetails() {
      return of(
        productDetails
      )
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, ModalComponent
      ],
      providers: [ {
        provide: ProductsService, useClass: MockProductService
      }],
    }).compileComponents();
  }));

  beforeEach(() => {    
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
    fixture.detectChanges();    
  })

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should render title in a h1 tag', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Product');
  });

  it('should have product-info', () => {
    expect(app.products).toEqual(productDetails.groups);
    expect(app.showModal).toEqual(false);
  });

  it('should close the modal initially ', () => {
    expect(app.showModal).toEqual(false);
  });

  it('should show the modal when clicking on image ', () => {
    const product = {
      href: '/images/test.jpg'
    };
    app.openModal(product);
    expect(app.selectedProduct).toEqual(product);
    expect(app.showModal).toEqual(true);
  });
});
